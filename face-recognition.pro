#-------------------------------------------------
#
# Project created by QtCreator 2017-08-19T20:52:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FaceRecognition
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += /usr/local/include

LIBS += \
#        /home/arnes/repos/dlib-19.4/dlib/build/libdlib.so.19.4.0 \
#        /home/arnes/repos/dlib-19.4/examples/dlib_build/libdlib.a \
        /usr/lib/libblas/libblas.so \
        /usr/lib/lapack/liblapack.so.3 \
        -ldlib \
#        -L/usr/local/cuda/lib64 -lcuda -lcudart -lcudnn \
        -L/usr/local/cuda-8.0/lib64 -lcudart \
        /usr/local/cuda-8.0/lib64/libcudnn.so.7 \
        -lcurand -lcublas -lcublas_device -lcudnn -lcudart_static \
        -L/usr/local/lib -lopencv_stitching -lopencv_superres \
        -lopencv_videostab -lopencv_aruco -lopencv_bgsegm -lopencv_bioinspired \
        -lopencv_ccalib -lopencv_dpm -lopencv_freetype -lopencv_fuzzy \
        -lopencv_line_descriptor -lopencv_optflow -lopencv_reg -lopencv_saliency \
        -lopencv_stereo -lopencv_structured_light -lopencv_phase_unwrapping \
        -lopencv_rgbd -lopencv_surface_matching -lopencv_tracking -lopencv_datasets \
        -lopencv_text -lopencv_face -lopencv_plot -lopencv_dnn -lopencv_xfeatures2d \
        -lopencv_shape -lopencv_video -lopencv_ximgproc -lopencv_calib3d \
        -lopencv_features2d -lopencv_flann -lopencv_xobjdetect \
        -lopencv_objdetect -lopencv_ml -lopencv_xphoto -lopencv_highgui \
        -lopencv_videoio -lopencv_imgcodecs -lopencv_photo -lopencv_imgproc -lopencv_core

QMAKE_CXXFLAGS += -DUSE_SSE4_INSTRUCTIONS=ON \
                    -DDLIB_USE_BLAS=ON \
                    -DDLIB_USE_LAPACK=ON

unix    {
    QMAKE_POST_LINK     +=  cp -r $${PWD}/distFiles/* $${OUT_PWD}
}

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    addnewfaceform.cpp \
    facerecognizer.cpp \
    unhandledexception.cpp \
    threadabort.cpp \
    recordvideoform.cpp \
    imageconversion.cpp \
    iframesource.cpp \
    facestorage.cpp \
    localfacestroge.cpp \
    facerecognitionform.cpp \
    framereadthread.cpp \
    recognizedfacewidget.cpp

HEADERS += \
        mainwindow.h \
    addnewfaceform.h \
    facerecognizer.h \
    unhandledexception.h \
    threadabort.h \
    recordvideoform.h \
    imageconversion.h \
    iframesource.h \
    facestorage.h \
    localfacestroge.h \
    facerecognitionform.h \
    framereadthread.h \
    recognizedfacewidget.h

FORMS += \
        mainwindow.ui \
    addnewfaceform.ui \
    recordvideoform.ui \
    facerecognitionform.ui \
    recognizedfacewidget.ui

RESOURCES += \
    icon.qrc
