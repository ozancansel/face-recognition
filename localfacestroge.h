#ifndef LOCALFACESTROGAE_H
#define LOCALFACESTROGAE_H

#include "facestorage.h"

#define     STORAGE_NAME_LABEL      "name"
#define     STORAGE_ID_LABEL        "id"
#define     STORAGE_ENCODING_LABEL  "encoding"

class LocalFaceStorage : public FaceStorage
{
public:

    static int      lastId();
    static int      newId();
    static void     refreshId();
    LocalFaceStorage(QObject *parent = nullptr);
    void                        save(FaceEncoding &face) override;
    QList<ComparingResult>      compare(dlib::matrix<float , 0 , 1> encoding , double minSimilarity) override;

private:

    static int      m_lastId;

};

#endif // LOCALFACESTROGAE_H
