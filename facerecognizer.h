#ifndef FACERECOGNIZER_H
#define FACERECOGNIZER_H

#include <opencv2/core.hpp>
#include <QObject>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/dnn.h>
#include <dlib/clustering.h>

using namespace std;
using namespace cv;
using namespace dlib;

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual = add_prev1<block<N,BN,1,tag1<SUBNET>>>;

template <template <int,template<typename>class,int,typename> class block, int N, template<typename>class BN, typename SUBNET>
using residual_down = add_prev2<avg_pool<2,2,2,2,skip1<tag2<block<N,BN,2,tag1<SUBNET>>>>>>;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block  = BN<con<N,3,3,1,1,relu<BN<con<N,3,3,stride,stride,SUBNET>>>>>;

template <int N, typename SUBNET> using ares      = relu<residual<block,N,affine,SUBNET>>;
template <int N, typename SUBNET> using ares_down = relu<residual_down<block,N,affine,SUBNET>>;

template <typename SUBNET> using alevel0 = ares_down<256,SUBNET>;
template <typename SUBNET> using alevel1 = ares<256,ares<256,ares_down<256,SUBNET>>>;
template <typename SUBNET> using alevel2 = ares<128,ares<128,ares_down<128,SUBNET>>>;
template <typename SUBNET> using alevel3 = ares<64,ares<64,ares<64,ares_down<64,SUBNET>>>>;
template <typename SUBNET> using alevel4 = ares<32,ares<32,ares<32,SUBNET>>>;

using anet_type = loss_metric<fc_no_bias<128,avg_pool_everything<
                            alevel0<
                            alevel1<
                            alevel2<
                            alevel3<
                            alevel4<
                            max_pool<3,3,2,2,relu<affine<con<32,7,7,2,2,
                            input_rgb_image_sized<150>
                            >>>>>>>>>>>>;

class FaceRecognizer : public QObject
{

    Q_OBJECT

public:

    FaceRecognizer(QObject* parent = nullptr);
    typedef struct{
        matrix<rgb_pixel>       face;
        matrix<float , 0 , 1>   descriptor;
        chip_details            details;
    } FaceDescriptor;

public slots:

    std::vector<FaceDescriptor>   identify(matrix<rgb_pixel> &image, cv::Size minCaptured);
    bool                saveFace(QString label, matrix<float , 0 , 1> &faceDescriptor);

private:

    shape_predictor         m_sp;
    anet_type               m_net;
    frontal_face_detector   m_detector;
    bool                    m_modelsLoaded;

    void                loadModels();
    void                mkpathFaces();

};

#endif // FACERECOGNIZER_H
