#include "recordvideoform.h"
#include "ui_recordvideoform.h"
#include <QFileDialog>
#include <QMessageBox>

RecordVideoForm::RecordVideoForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RecordVideoForm) ,
    m_recordThread(nullptr)
{
    ui->setupUi(this);
    connect(ui->recordButton , SIGNAL(clicked(bool)) , this , SLOT(startRecording(bool)));
    connect(ui->browseButton , SIGNAL(clicked(bool)) , this , SLOT(askRecordingFile(bool)));
}

RecordVideoForm::~RecordVideoForm()
{
    delete ui;
}

void  RecordVideoForm::startRecording(bool enabled){
    if(enabled){

        if(m_recordThread != nullptr){
            warning("Zaten su anda kayittasiniz");
            ui->recordButton->setChecked(false);
            return;
        }

        if(m_saveFile.isEmpty()){
            warning("Kayit yerini secmediniz.");
            ui->recordButton->setChecked(false);
            return;
        }

        m_recordThread =  new RecordVideoThread();
        m_recordThread->setFps(30);
        m_recordThread->setRecordingFile(m_saveFile);
        connect(m_recordThread , SIGNAL(finished()) , m_recordThread , SLOT(deleteLater()));
        connect(m_recordThread , SIGNAL(recordError(QString)) , this , SLOT(recordError(QString)));
        connect(m_recordThread , SIGNAL(recordSuccess(QString)), this , SLOT(recordSuccess(QString)));
        connect(m_recordThread , SIGNAL(frameCaptured(Mat*)) , this , SLOT(frameCaptured(Mat*)));
        m_recordThread->start();
        ui->recordButton->setText("Kaydi Bitir");
    }   else    {
        if(m_recordThread != nullptr){
            m_recordThread->abort();
            m_recordThread = nullptr;
        }
        ui->recordButton->setText("Kaydi Baslat");
    }
}

void RecordVideoForm::askRecordingFile(bool enabled){
    Q_UNUSED(enabled)
    QString fileName = QFileDialog::getSaveFileName(this , "Video Kayit Yerini Seciniz");

    if(!fileName.isEmpty())
        m_saveFile = fileName;
}

void RecordVideoForm::warning(QString message){
    QMessageBox* box = new QMessageBox(this);
    box->setWindowTitle("Uyari!");
    box->setIcon(QMessageBox::Warning);
    box->setText(message);
    box->show();
}

void RecordVideoForm::error(QString message){
    QMessageBox *box = new QMessageBox();
    box->setWindowTitle("Hata!");
    box->setIcon(QMessageBox::Critical);
    box->setText(message);
    box->show();
}

void RecordVideoForm::info(QString message){
    QMessageBox *box = new QMessageBox();
    box->setWindowTitle("Bilgilendirme");
    box->setIcon(QMessageBox::Information);
    box->setText(message);
    box->show();
}

void RecordVideoForm::recordError(QString error){
    RecordVideoForm::error(error);
}

void RecordVideoForm::recordSuccess(QString success){
    info(success);
}

void RecordVideoForm::frameCaptured(Mat *frame){
    QImage img = ImageConversion::Mat2QImage(*frame);
    ui->frameDisplay->setPixmap(QPixmap::fromImage(img));
}

//RecordVideo thread implemenatation
void RecordVideoThread::run(){

    VideoWriter     writer;
    VideoCapture    cap;

    if(m_fileName.isEmpty()){
        emit recordError("Kayit edilecek dosya belirtilmemis");
        return;
    }

    if(m_fps < 0)
        m_fps = 30;

    if(!writer.open(m_fileName.toStdString() , CV_FOURCC('M','P','E','G') , m_fps , Size(640 , 480))){
        emit recordError("Writer acilamiyor");
        return;
    }

    if(!cap.open(0)){
        emit recordError("Kamera acilamiyor.");
        return;
    }


    cap.set(CV_CAP_PROP_FRAME_WIDTH , 640);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT , 480);

    int period = 1000.0 / (double)m_fps;
    Mat frame;
    while(!isAborted()){
        cap.read(frame);

        emit frameCaptured(&frame);
        writer.write(frame);
        QThread::msleep(period);
    }

    writer.release();
    cap.release();
    emit  recordSuccess("Video basariyla yazildi.");
}

void RecordVideoThread::setFps(int fps){
    m_fps = fps;
}

void   RecordVideoThread::setRecordingFile(QString fileName){
    m_fileName = fileName;
}
