#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "addnewfaceform.h"
#include "recordvideoform.h"
#include "facerecognitionform.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionYeni_Kisi_Ekle, SIGNAL(triggered(bool)) , this , SLOT(openNewFaceForm(bool)));
    connect(ui->actionKayit , SIGNAL(triggered(bool)) , this , SLOT(openRecordingForm(bool)));
    connect(ui->actionYuz_Tanima , SIGNAL(triggered(bool)) , this , SLOT(openFaceRecognitionForm(bool)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::openNewFaceForm(bool val){
    Q_UNUSED(val)
    clearLayout(ui->formLayout);

    AddNewFaceForm* form = new AddNewFaceForm(statusBar(),this);
    ui->formLayout->addWidget(form);
}

void MainWindow::openRecordingForm(bool enabled){
    Q_UNUSED(enabled)

    clearLayout(ui->formLayout);

    RecordVideoForm*    form = new RecordVideoForm(this);
    ui->formLayout->addWidget(form);
}

void MainWindow::openFaceRecognitionForm(bool enabled){
    Q_UNUSED(enabled)

    clearLayout(ui->formLayout);

    FaceRecognitionForm*    form = new FaceRecognitionForm(this);
    ui->formLayout->addWidget(form);
}

void MainWindow::clearLayout(QLayout* layout){
    QLayoutItem *item;
     while((item = layout->takeAt(0))) {
         if (item->layout()) {
             clearLayout(item->layout());
             delete item->layout();
         }
         if (item->widget()) {
             delete item->widget();
         }
         delete item;
     }
}
