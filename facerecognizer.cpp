#include "facerecognizer.h"
#include <QApplication>
#include <QDir>
#include "unhandledexception.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QCryptographicHash>
#include <QTextStream>
#include <QTime>

FaceRecognizer::FaceRecognizer(QObject* parent) :
    QObject(parent) ,
    m_modelsLoaded(false)
{
}

std::vector<FaceRecognizer::FaceDescriptor>  FaceRecognizer::identify(matrix<rgb_pixel> &image ,
                                                                      cv::Size minFaceRect){
    //Deep neutral network model yukleniyor
    if(!m_modelsLoaded)
        loadModels();

    std::vector<matrix<rgb_pixel>>  faces;
    std::vector<dlib::rectangle>    dets = m_detector(image , 0);
    std::vector<chip_details>       chipDetails;

    for(dlib::rectangle face : dets){
        if(minFaceRect.height > 0 && face.height() < (unsigned long)minFaceRect.height){
            qDebug() << "Frame passing height is too small";
            continue;
        }

        if(minFaceRect.width > 0 &&  face.width() < (unsigned long)minFaceRect.width){
            qDebug() << "Frame passing width is too small";
            continue;
        }

        auto shape = m_sp(image , face);
        matrix<rgb_pixel>   face_chip;
        chip_details   details = get_face_chip_details(shape , 150 , 0.25);
        chipDetails.push_back(details);
        extract_image_chip(image , details , face_chip);
        faces.push_back(face_chip);
    }

    std::vector<FaceDescriptor> descriptors;
    //Yuz bulunamadiysa
    if(faces.empty()){
        return descriptors;
    }

    std::vector<matrix<float , 0 , 1>>  faceDescriptors = m_net(faces);

    for(size_t i = 0; i < faceDescriptors.size(); i++){
        FaceDescriptor  descriptor;
        descriptor.descriptor = faceDescriptors.at(i);
        descriptor.face = faces.at(i);
        descriptor.details = chipDetails.at(i);
        descriptors.push_back(descriptor);
    }

    return descriptors;
}

bool FaceRecognizer::saveFace(QString label, matrix<float, 0, 1> &faceDescriptor){
    QJsonArray array;

    for(auto val : faceDescriptor){
        array.append(QJsonValue(val));
    }

    QByteArray json = QJsonDocument(array).toJson();
    QByteArray encodedJson = json.toHex();

    mkpathFaces();
    QFile   f(QString("%0/faces/%0.fd").
              arg(QApplication::applicationDirPath()).
              arg(label));

    if(!f.open(QFile::WriteOnly | QFile::Truncate))//Dosya acilamazsa hata firlatiliyor
        UnhandledException(QString("%0 dosyasi yazilamiyor.").arg(f.fileName())).raise();

    if(!f.write(encodedJson))//Dosya yazilamazsa hata firlatiliyor
        UnhandledException(QString("%0 dosyasi yazilamiyor.").arg(f.fileName())).raise();


    return true;
}

void FaceRecognizer::loadModels(){
    QDir dir(QApplication::applicationDirPath());

    if(!dir.exists("models/shape_predictor_68_face_landmarks.dat"))
        UnhandledException(QString("models/shape_predictor_68_face_landmarks.dat bulunamiyor.")).raise();
    if(!dir.exists("models/dlib_face_recognition_resnet_model_v1.dat"))
        UnhandledException(QString("models/dlib_face_recognition_resnet_model_v1.dat bulunamiyor")).raise();

    deserialize("models/shape_predictor_68_face_landmarks.dat") >> m_sp;
    deserialize("models/dlib_face_recognition_resnet_model_v1.dat") >> m_net;
    m_detector = get_frontal_face_detector();
    m_modelsLoaded = true;
}

void FaceRecognizer::mkpathFaces(){
    QDir    facesDir(QString("%0/faces").arg(QApplication::applicationDirPath()));

    if(!facesDir.exists())
        facesDir.mkpath(facesDir.absolutePath());
}
