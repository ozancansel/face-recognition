#include "threadabort.h"

ThreadAbort::ThreadAbort() :
    m_aborted(false) ,
    m_paused(false)
{

}

void ThreadAbort::pause(){
    m_paused = true;
}

void ThreadAbort::resume(){
    m_paused = false;
}

void ThreadAbort::abort(){
    m_aborted = true;
}

bool ThreadAbort::isAborted(){
    return m_aborted;
}

bool ThreadAbort::isPaused(){
    return m_paused;
}
