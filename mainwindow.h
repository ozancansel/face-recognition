#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:

    void        openNewFaceForm(bool val);
    void        openRecordingForm(bool enabled);
    void        openFaceRecognitionForm(bool enabled);

private:

    Ui::MainWindow *ui;
    void        clearLayout(QLayout* layout);

};

#endif // MAINWINDOW_H
