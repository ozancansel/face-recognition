#ifndef FACERECOGNITIONFORM_H
#define FACERECOGNITIONFORM_H

#include <QWidget>
#include <QHBoxLayout>
#include <opencv2/highgui.hpp>
#include <dlib/opencv.h>
#include <dlib/pixel.h>
#include <dlib/image_processing.h>
#include "threadabort.h"
#include "framereadthread.h"
#include "facestorage.h"
#include "localfacestroge.h"

namespace Ui {
class FaceRecognitionForm;
}

class RecognizeThread : public ThreadAbort{

    Q_OBJECT

public:
    RecognizeThread(FrameReadThread* readThread);

protected:

    void    run();

signals:

    void    faceRecognized(double similarity , QString label , cv::Mat *frame);



private:

    FrameReadThread*    m_readThread;

};

class FaceRecognitionForm : public QWidget
{

    Q_OBJECT

public:

    explicit FaceRecognitionForm(QWidget *parent = 0);
    ~FaceRecognitionForm();

public slots:

    void        startRecognition(bool enabled);
    void        faceRecognized(double similarity , QString label , cv::Mat *frame);
    void        frameCaptured(cv::Mat *frame);

private:

    Ui::FaceRecognitionForm *ui;
    FaceStorage*        m_storage;
    QHBoxLayout*        m_recognizedFacesLayout;
    FrameReadThread*    m_readThread;
    RecognizeThread*    m_recognizedThread;

};

#endif // FACERECOGNITIONFORM_H
