#ifndef RECORDVIDEOFORM_H
#define RECORDVIDEOFORM_H

#include <QWidget>
#include <opencv2/highgui.hpp>
#include "threadabort.h"
#include "imageconversion.h"

using namespace cv;

namespace Ui {
class RecordVideoForm;
}

class RecordVideoThread : public ThreadAbort{
    Q_OBJECT

public:

    void    setRecordingFile(QString fileName);
    void    setFps(int fps);

signals:

    void    recordError(QString message);
    void    recordSuccess(QString message);
    void    frameCaptured(Mat* frame);

protected:

    void    run();

private:

    QString     m_fileName;
    int         m_fps;
};

class RecordVideoForm : public QWidget
{
    Q_OBJECT

public:

    explicit RecordVideoForm(QWidget *parent = 0);
    ~RecordVideoForm();

public slots:

    void        startRecording(bool enabled);
    void        askRecordingFile(bool enabled);
    void        recordError(QString error);
    void        recordSuccess(QString success);
    void        frameCaptured(Mat *frame);

signals:


private:

    Ui::RecordVideoForm *ui;
    QString             m_saveFile;
    RecordVideoThread*  m_recordThread;

    void                warning(QString message);
    void                error(QString message);
    void                info(QString message);
};

#endif // RECORDVIDEOFORM_H
