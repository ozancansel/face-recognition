#ifndef FACESTORAGE_H
#define FACESTORAGE_H

#include <QObject>
#include <QImage>
#include <dlib/matrix.h>

class FaceStorage : public QObject
{

    Q_OBJECT

public:

    typedef struct{
        int                         id;
        QString                     name;
        dlib::matrix<float , 0 , 1> encoding;
        QImage                      image;
    } FaceEncoding;

    typedef struct{
        double      similarity;
        QString     name;
        int         id;
    } ComparingResult;

    FaceStorage(QObject *parent = nullptr);
    virtual void                        save(FaceEncoding &face) = 0;
    virtual QList<ComparingResult>      compare(dlib::matrix<float , 0 , 1> encoding , double minSimilarity) = 0;
};

#endif // FACESTORAGE_H
