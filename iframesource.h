#ifndef IFRAMESOURCE_H
#define IFRAMESOURCE_H

#include <opencv2/core.hpp>
#include <QMutex>

using namespace cv;

class IFrameSource
{

public:

    QMutex&     frameMutex();
    virtual     Mat&  read() = 0;

private:

    QMutex      m_frameMutex;

};

#endif // IFRAMESOURCE_H
