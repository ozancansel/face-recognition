#ifndef THREADABORT_H
#define THREADABORT_H

#include <QThread>

class ThreadAbort : public QThread
{

public:

    ThreadAbort();
    bool    isAborted();
    bool    isPaused();
    void    pause();
    void    resume();

public slots:

    void    abort();

private:

    bool    m_aborted;
    bool    m_paused;

};

#endif // THREADABORT_H
