#include "framereadthread.h"

FrameReadThread::FrameReadThread(QString fileName)  :
    m_fileName(fileName) ,
    m_sync(true)
{

}

FrameReadThread::FrameReadThread(int camIdx, bool sync) :
    m_camIdx(camIdx) ,
    m_sync(sync)
{

}

void FrameReadThread::run(){
    using namespace cv;

    if(!m_fileName.isEmpty())
        m_cap.open(m_fileName.toStdString());
    else
        m_cap.open(m_camIdx);

    if(!m_cap.isOpened()){
        emit cameraCannotBeOpened();
        return;
    }

    //Kamera ayarlari yapiliyor
    m_cap.set(CV_CAP_PROP_FRAME_WIDTH , m_capWidth);
    m_cap.set(CV_CAP_PROP_FRAME_HEIGHT , m_capHeight);

    while(!isAborted()){
        //Frame'e ayni anda erisilmemesi icin mutext kitleniyor
        if(m_sync){
            QThread::msleep(50);
            continue;
        }
        if(frameMutex().tryLock()){
            m_cap >> m_frame;
            emit frameCaptured(&m_frame);
            //Kilit acicliyor
            frameMutex().unlock();
            //Eger ki baska bir erisim varsa thread uyutuluyor
            QThread::msleep(5);
        }
    }

    //Kamera kapatiliyor
    m_cap.release();
}

cv::Mat& FrameReadThread::read(){

    if(m_sync && m_cap.isOpened()){
        frameMutex().tryLock();
        m_cap.read(m_frame);
        frameMutex().unlock();
        emit frameCaptured(&m_frame);
    }

    return m_frame;
}

QMutex& FrameReadThread::frameMutex(){
    return m_frameMutex;
}

void FrameReadThread::setCaptureWidth(int width){
    m_capWidth = width;
}

void FrameReadThread::setCaptureHeight(int height){
    m_capHeight = height;
}

