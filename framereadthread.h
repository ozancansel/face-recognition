#ifndef FRAMEREADTHREAD_H
#define FRAMEREADTHREAD_H

#include <QMutex>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>
#include "threadabort.h"

class FrameReadThread : public ThreadAbort {

    Q_OBJECT

public:

    FrameReadThread(QString fileName);
    FrameReadThread(int camIdx = 0 , bool sync = false);
    void        setCaptureWidth(int width);
    void        setCaptureHeight(int height);
    cv::Mat&        read();
    QMutex&     frameMutex();

signals:

    void        cameraCannotBeOpened();
    void        frameCaptured(cv::Mat *frame);

protected:

    void    run();

private:
    cv::VideoCapture    m_cap;
    cv::Mat             m_frame;
    int                 m_capWidth;
    int                 m_capHeight;
    int                 m_camIdx;
    QString             m_fileName;
    bool                m_sync;
    QMutex              m_frameMutex;


};

#endif // FRAMEREADTHREAD_H
