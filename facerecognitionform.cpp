#include "facerecognitionform.h"
#include "ui_facerecognitionform.h"
#include "facerecognizer.h"
#include "imageconversion.h"
#include "recognizedfacewidget.h"
#include <QDebug>

FaceRecognitionForm::FaceRecognitionForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FaceRecognitionForm) ,
    m_storage(new LocalFaceStorage(this)) ,
    m_recognizedThread(nullptr),
    m_readThread(nullptr)
{
    ui->setupUi(this);
    m_recognizedFacesLayout = new QHBoxLayout();
    ui->recognizedFacesWidget->setLayout(m_recognizedFacesLayout);

    connect(ui->startRecognitionButton , SIGNAL(clicked(bool)) , this , SLOT(startRecognition(bool)));
}

FaceRecognitionForm::~FaceRecognitionForm()
{
    delete ui;
}

void FaceRecognitionForm::startRecognition(bool enabled){
    Q_UNUSED(enabled)

    m_readThread = new FrameReadThread();
    m_recognizedThread = new RecognizeThread(m_readThread);

    m_readThread->setCaptureWidth(320);
    m_readThread->setCaptureHeight(240);

    connect(m_recognizedThread , SIGNAL(faceRecognized(double,QString,cv::Mat*)) , this , SLOT(faceRecognized(double,QString,cv::Mat*)));
    connect(m_recognizedThread , SIGNAL(finished()) , this , SLOT(deleteLater()));
    connect(m_readThread , SIGNAL(finished()) , m_readThread , SLOT(deleteLater()));
    connect(m_readThread , SIGNAL(frameCaptured(cv::Mat*)) , this , SLOT(frameCaptured(cv::Mat*)));
    m_readThread->start();
    m_recognizedThread->start();
}

void FaceRecognitionForm::faceRecognized(double similarity, QString label, Mat *frame){
    m_recognizedThread->pause();
//    Mat cvMat = dlib::toMat(*frame);

    QImage img = ImageConversion::Mat2QImage(*frame);

    RecognizedFaceWidget *widget = new RecognizedFaceWidget();
    widget->setLabel(label);
    widget->setImage(img);
    widget->setSimilarity(similarity);
    m_recognizedFacesLayout->addWidget(widget);

    //Layout dolduysa en eski yuz siliniyor
    if(m_recognizedFacesLayout->count() > 5){
        QLayoutItem* item = m_recognizedFacesLayout->itemAt(0);;
        item->widget()->deleteLater();
        m_recognizedFacesLayout->removeItem(item);
    }
    m_recognizedThread->resume();
}

void FaceRecognitionForm::frameCaptured(Mat *frame){
    QImage img = ImageConversion::Mat2QImage(*frame);

    QPixmap pixmap = QPixmap::fromImage(img);
    ui->streamDisplay->setPixmap(pixmap);
}

//RecognizeThread
RecognizeThread::RecognizeThread(FrameReadThread *readThread) :
                m_readThread(readThread)
{

}

void RecognizeThread::run(){
    using namespace cv;
    using namespace dlib;
    using namespace std;
    Mat             frame;
    FaceRecognizer  recognizer;
    while(!isAborted()){

        if(isPaused()){
            QThread::msleep(10);
            continue;
        }

        if(m_readThread->frameMutex().tryLock())
            continue;
        frame = m_readThread->read().clone();
        m_readThread->frameMutex().unlock();

        if(frame.empty()){
            QThread::msleep(100);
            continue;
        }

        matrix<rgb_pixel> dlibFrame = mat(cv_image<rgb_pixel>(frame));
        std::vector<FaceRecognizer::FaceDescriptor> descriptors =  recognizer.identify(dlibFrame , Size(-1 , -1));

        if(descriptors.size() == 1){
            qDebug() << "Face recognized " << "Ozan";
            FaceRecognizer::FaceDescriptor descriptor = descriptors.at(0);
            Mat m = dlib::toMat(descriptor.face);
            emit faceRecognized(0.6 , "Ozan" , &m);
        }
    }
}
