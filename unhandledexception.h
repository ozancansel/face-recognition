#ifndef UNHANDLEDEXCEPTION_H
#define UNHANDLEDEXCEPTION_H

#include <QException>
#include <QDebug>

class UnhandledException : public QException
{

public:

    UnhandledException(QString message) :
        m_message(message)
    {   }
    UnhandledException()
    {  }

    void    raise() const {
        if(!m_message.isEmpty())
            qCritical() << "UnhandledException message " << m_message;
        throw *this;
    }
    UnhandledException *clone() const { return new UnhandledException(*this); }
    QString     getMessage(){
        return m_message;
    }

private:

    QString     m_message;

};

#endif // UNHANDLEDEXCEPTION_H
