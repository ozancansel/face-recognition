#ifndef RECOGNIZEDFACEWIDGET_H
#define RECOGNIZEDFACEWIDGET_H

#include <QWidget>

namespace Ui {
class RecognizedFaceWidget;
}

class RecognizedFaceWidget : public QWidget
{
    Q_OBJECT

public:
    explicit RecognizedFaceWidget(QWidget *parent = 0);
    ~RecognizedFaceWidget();
    void    setSimilarity(double val);
    void    setLabel(QString label);
    void    setImage(QImage &image);

private:

    Ui::RecognizedFaceWidget *ui;

};

#endif // RECOGNIZEDFACEWIDGET_H
