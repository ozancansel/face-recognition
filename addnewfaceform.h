#ifndef ADDNEWFACEFORM_H
#define ADDNEWFACEFORM_H

#include <QWidget>
#include <QThread>
#include <QMutex>
#include <QStatusBar>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <dlib/pixel.h>
#include <dlib/image_processing.h>
#include "threadabort.h"
#include "iframesource.h"
#include "facestorage.h"
#include "facerecognizer.h"
#include "localfacestroge.h"
#include "framereadthread.h"

namespace Ui {
class AddNewFaceForm;
}

class IdentifyFacesThread : public ThreadAbort{
    Q_OBJECT

public:

    IdentifyFacesThread(QString m_file);
    IdentifyFacesThread(int camIdx);
    FrameReadThread*    cameraThread();
    void        pause(bool enabled);

protected:

    void        run();

signals:

    void        faceCaptured(FaceRecognizer::FaceDescriptor *descriptor);

private:

    FrameReadThread* m_cameraThread;
    void        applySignalSlot();
    bool        m_paused;

};

class AddNewFaceForm : public QWidget
{
    Q_OBJECT

public:
    explicit AddNewFaceForm(QStatusBar* statusBar , QWidget *parent = 0);
    ~AddNewFaceForm();

public slots:

    void    startToSave(bool enabled);
    void    webcamFrameCaptured(cv::Mat *frame);
    void    faceCaptured(FaceRecognizer::FaceDescriptor *descriptor);
    void    askVideoPath(bool enabled);
    void    messageReceived(QString msg);

private:

    Ui::AddNewFaceForm      *ui;
    IdentifyFacesThread*    m_currentThread;
    QString                 m_videoFile;
    QStatusBar*             m_statusBar;
    FaceStorage*            m_storage;

    void                warning(QString message);
    void                info(QString message);
    void                error(QString message);


};

#endif // ADDNEWFACEFORM_H
