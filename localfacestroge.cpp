#include "localfacestroge.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDir>
#include <QApplication>
#include "unhandledexception.h"

int LocalFaceStorage::m_lastId = 0;

int LocalFaceStorage::lastId(){
    return m_lastId;
}

int LocalFaceStorage::newId(){
    return m_lastId + 1;
}

void LocalFaceStorage::refreshId(){
    QDir    dir(QString("%0/localStorage/encodings").arg(QApplication::applicationDirPath()));

    int     lastId = 0;
    foreach(QString fileName , dir.entryList(QDir::NoDotAndDotDot)){
        QStringList fileParts = fileName.split("-");
        QString idStr = fileParts.at(1);//isimlendirme formati <isim>-<id> oldugu icin 2. parametre idyi ifade eder.
        int id = idStr.toInt();

        if(id > lastId)
            lastId = id;
    }

    m_lastId = lastId;
}

LocalFaceStorage::LocalFaceStorage(QObject *parent) : FaceStorage(parent){

}

void LocalFaceStorage::save(FaceEncoding &face){
    QJsonObject obj;
    QJsonArray  encodingArray;

    //Eger id atanmamissa
    if(face.id < 0){
        face.id = newId();
    }

    QString     imageFile = QString("%0/localStorage/faces/%1.jpg").
            arg(QApplication::applicationDirPath()).
            arg(QString::number(face.id));

    //Resim kaydediliyor
    if(!face.image.save(imageFile,"jpg" , 100)){
        //Resim yazilamazsa hata firlatiliyor
        UnhandledException("Resim dosyaya yazilamadi.").raise();
    }

    for(double val : face.encoding){
        encodingArray.append(val);
    }

    obj[STORAGE_NAME_LABEL] = face.name;
    obj[STORAGE_ID_LABEL] = face.id;
    obj[STORAGE_ENCODING_LABEL] = encodingArray;

    QByteArray jsonBinary = QJsonDocument(obj).toJson(QJsonDocument::Compact);

#ifndef Qt_DEBUG
    jsonBinary = jsonBinary.toBase64();
#endif

    QString jsonFileName(QString("%0/localStorage/encodings/%1-%2")
                         .arg(QApplication::applicationDirPath())
                         .arg(face.name)
                         .arg(face.id));

    QFile   jsonFile(jsonFileName);

    if(!jsonFile.open(QIODevice::WriteOnly | QIODevice::Truncate)){
        //Resim siliniyor
        QFile(imageFile).remove();
        UnhandledException("Json dosyasini yazilamadi.").raise();
    }

    //Dosyaya yaziliyor
    jsonFile.write(jsonBinary);
}

QList<LocalFaceStorage::ComparingResult> LocalFaceStorage::compare(dlib::matrix<float, 0, 1> encoding, double minSimilarity){

}
