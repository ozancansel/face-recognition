#include "recognizedfacewidget.h"
#include "ui_recognizedfacewidget.h"
#include <QPixmap>

RecognizedFaceWidget::RecognizedFaceWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RecognizedFaceWidget)
{
    ui->setupUi(this);
}

RecognizedFaceWidget::~RecognizedFaceWidget()
{
    delete ui;
}

void RecognizedFaceWidget::setSimilarity(double val){
    ui->similarityLabel->setText(QString::number(val));
}

void RecognizedFaceWidget::setLabel(QString label){
    ui->nameLabel->setText(label);
}

void RecognizedFaceWidget::setImage(QImage &image){
    QPixmap pix = QPixmap::fromImage(image);

    ui->imageDisplay->setPixmap(pix);
}
