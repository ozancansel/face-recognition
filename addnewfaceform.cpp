#include "addnewfaceform.h"
#include "ui_addnewfaceform.h"
#include <dlib/xml_parser.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <dlib/geometry.h>
#include <dlib/opencv.h>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QTime>
#include "facerecognizer.h"
#include "imageconversion.h"

AddNewFaceForm::AddNewFaceForm(QStatusBar* statusBar, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AddNewFaceForm) ,
    m_currentThread(nullptr) ,
    m_statusBar(statusBar) ,
    m_storage(new LocalFaceStorage(this))
{
    ui->setupUi(this);
    connect(ui->kayitiBaslat , SIGNAL(clicked(bool)) , this , SLOT(startToSave(bool)));
    connect(ui->videoDosyasiniSec , SIGNAL(clicked(bool)) , this , SLOT(askVideoPath(bool)));
}

AddNewFaceForm::~AddNewFaceForm()
{
    delete ui;
}

void AddNewFaceForm::startToSave(bool enabled){
    Q_UNUSED(enabled)

    //Eger thread nullsa baslatiliyor
    if(m_currentThread == nullptr){
        if(ui->dosyadanOku->isChecked()){
            //Video dosyasi kontrol ediliyor
            if(m_videoFile.isEmpty()){
                warning("Lutfen dosyayi seciniz");
                return;
            }

            m_currentThread = new IdentifyFacesThread(m_videoFile);
        }   else if(ui->kameradanOku->isChecked())  {
            m_currentThread = new IdentifyFacesThread(0);
        }
        connect(m_currentThread , SIGNAL(finished()), m_currentThread , SLOT(deleteLater()));
        connect(m_currentThread->cameraThread() , SIGNAL(frameCaptured(cv::Mat*)) , this , SLOT(webcamFrameCaptured(cv::Mat*)));
        connect(m_currentThread , SIGNAL(faceCaptured(FaceRecognizer::FaceDescriptor*)), this , SLOT(faceCaptured(FaceRecognizer::FaceDescriptor*)));
        connect(m_currentThread , SIGNAL(message(QString)) , this , SLOT(messageReceived(QString)));
    }

    //Thread baslatiliyor
    m_currentThread->start(QThread::TimeCriticalPriority);
}

void AddNewFaceForm::messageReceived(QString msg){
    if(m_statusBar == nullptr)
        return;

    m_statusBar->showMessage(msg , 2000);
}

void AddNewFaceForm::webcamFrameCaptured(cv::Mat *frame){
    QImage img = ImageConversion::Mat2QImage(*frame);

    QPixmap pixmap = QPixmap::fromImage(img);
    ui->webcamStreamDisplay->setPixmap(pixmap);
}

void AddNewFaceForm::faceCaptured(FaceRecognizer::FaceDescriptor *descriptor){
    m_currentThread->pause(true);
    Mat mFrame = dlib::toMat(descriptor->face);
    QImage img = ImageConversion::Mat2QImage(mFrame);

    QPixmap pixmap = QPixmap::fromImage(img);
    ui->capturedFaceDisplay->setPixmap(pixmap);

    double angleInDegree = descriptor->details.angle / CV_PI * 180;
    QString instruction;
    if(qAbs(angleInDegree) > 5){
        if(angleInDegree > 0)
            instruction = QString("Lutfen basinizi hafifce sola cevirin. Aci %0°").arg(angleInDegree);
        else if(angleInDegree < 0)
            instruction = QString("Lutfen basinizi hafifce saga cevirin. Aci %0°").arg(angleInDegree);

        m_statusBar->showMessage(instruction);
        m_currentThread->pause(false);
    } else  {
        instruction = "Yuz dogrulandi. Lutfen hareket etmeyin.";
        m_statusBar->showMessage(instruction);
        QMessageBox* saveUser = new QMessageBox;
        saveUser->setWindowTitle("Kullaniciyi eklemek istiyor musunuz ?");
        saveUser->setIcon(QMessageBox::Question);
        saveUser->setText("Yuz haritasi cikartildi, kullaniciyi eklemek istiyor musunuz ?");
        saveUser->setStandardButtons(QMessageBox::Yes | QMessageBox::No);

        QMessageBox::StandardButton reply = QMessageBox::question(this , "Kullaniciyi eklemek istiyor musunuz ?" ,
                              "Yuz haritasi cikartildi, kullaniciyi eklemek istiyor musunuz ?",
                              QMessageBox::Yes | QMessageBox::No);
        //Eger frame kabul edildiyse
        if(reply == QMessageBox::Yes){

            if(ui->isimTextBox->text().isEmpty()){
                warning("Lutfen isim giriniz");

                m_currentThread->pause(false);
                return;
            }
            FaceStorage::FaceEncoding   encoding;

            encoding.encoding = descriptor->descriptor;
            encoding.name = ui->isimTextBox->text();
            encoding.id = -1;
            encoding.image = img;
            m_storage->save(encoding);

            m_currentThread->abort();
        } else {
            m_currentThread->pause(false);
        }
    }

    if(qAbs(angleInDegree) < 5){
    }
}

void AddNewFaceForm::askVideoPath(bool enabled){
    Q_UNUSED(enabled)
    QString fileName  = QFileDialog::getOpenFileName(this , "Videoyu secin");
    if(!fileName.isEmpty())
        m_videoFile = fileName;
}

void AddNewFaceForm::info(QString message){
    QMessageBox* box = new QMessageBox(this);
    box->setWindowTitle("Bilgilendirme");
    box->setIcon(QMessageBox::Warning);
    box->setText(message);
    box->show();
}

void AddNewFaceForm::warning(QString message){
    QMessageBox* box = new QMessageBox(this);
    box->setWindowTitle("Uyari!");
    box->setIcon(QMessageBox::Warning);
    box->setText(message);
    box->show();
}

void AddNewFaceForm::error(QString message){
    QMessageBox* box = new QMessageBox(this);
    box->setWindowTitle("Hata!");
    box->setIcon(QMessageBox::Critical);
    box->setText(message);
    box->show();
}

//AddNewFaceThread implementation
IdentifyFacesThread::IdentifyFacesThread(QString m_file) :
    m_paused(false)
{
    m_cameraThread = new FrameReadThread(m_file);
}

IdentifyFacesThread::IdentifyFacesThread(int camIdx) :
    m_paused(false)
{
    m_cameraThread = new FrameReadThread(camIdx);
}

void IdentifyFacesThread::applySignalSlot(){
    connect(m_cameraThread , SIGNAL(cameraCannotBeOpened()) , this , SLOT(abort()));
    connect(m_cameraThread , SIGNAL(finished()) , m_cameraThread, SLOT(deleteLater()));
}

void IdentifyFacesThread::run(){
    using namespace cv;
    using namespace dlib;
    using namespace std;

    m_cameraThread->setCaptureWidth(640);
    m_cameraThread->setCaptureHeight(480);
    m_cameraThread->start();

    Mat frame;
    FaceRecognizer  recognizer;

    //Alinan frame'e gore yuz tanima yapilacagi icin mutex kitlenip frame aliniyor
    while(!isAborted()){

        //DUrduruldugu icin pas geciliyor
        if(m_paused){
            QThread::msleep(10);
            continue;
        }

        //Eger kameradan frame okunuyorsa pas geciliyor
        if(!m_cameraThread->frameMutex().tryLock()) //Mutex kitleniyor
            continue;

        frame = m_cameraThread->read().clone();

        //Mutex aciliyor
        m_cameraThread->frameMutex().unlock();

        //Eger alinan frame bossa, muhtemelen kamera aciliyordur o yuzden thread 100 milisaniye uyutuluyor
        if(frame.empty()){
            QThread::msleep(100);
            continue;
        }

        matrix<rgb_pixel> dlibFrame = mat(cv_image<rgb_pixel>(frame));
        std::vector<FaceRecognizer::FaceDescriptor> faceDescriptors = recognizer.identify(dlibFrame , cv::Size(200 , -1));

        if(faceDescriptors.size() == 1){
            FaceRecognizer::FaceDescriptor  first = faceDescriptors.at(0);

            m_paused = true;
            emit faceCaptured(&first);
        }
    }

    m_cameraThread->abort();
    if(!m_cameraThread->wait(3000)){
        m_cameraThread->terminate();
        m_cameraThread->wait();
    }
}

void IdentifyFacesThread::pause(bool enabled){
    m_paused = enabled;
}

FrameReadThread* IdentifyFacesThread::cameraThread(){
    return m_cameraThread;
}
